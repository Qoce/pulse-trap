Adds a pulse trap to Synchrony and generates one of them at a random (non-shop, non-T3) wall in every zone 4 floor.
The wall that is selceted is upgraded to tier 3.
The pulse trap is very similar to the firepig, except that it shoots pulse in all 4 directions. It charges and activates when the player is within the 2 tile "X" sign that pulse makes, regardless of whether it is lit or not.
