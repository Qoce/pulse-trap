--This mod adds a "pulse snake", that functions like a firepig except that it casts a pulse spell 
--It activiates when a player is within two tiles
--Currently set to cast a regular 4 way directional pulse
--Much of this is heavily adapted from the firepig code in synchrony
--This is because they are supposed to fundamentally the same type of trap, so I want their behavior to be consistent

local event = require "necro.event.Event"
local components = require "necro.game.data.Components"
local ecs = require "system.game.Entities"
local customEntities = require "necro.game.data.CustomEntities"
local commonTrap = require "necro.game.data.trap.CommonTrap"
local collision = require "necro.game.tile.Collision"
local action = require "necro.game.system.Action"
local constant = components.constant
local team = require "necro.game.character.Team"
local enemy = require "necro.game.data.enemy.CommonEnemy"
local vision = require "necro.game.vision.Vision"
local targeting = require "necro.game.enemy.Targeting"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local attack = require "necro.game.character.Attack"
local tile = require "necro.game.tile.Tile"
local object = require "necro.game.object.Object"
local currentLevel = require "necro.game.level.CurrentLevel"
local rng = require "necro.game.system.RNG"

local max = math.max
local abs = math.abs

--Pulse tiles
local targetTiles = {
	offsets = {
		{  1,  1 }, {  2,  2 },
		{  1, -1 }, {  2, -2 },
		{ -1,  1 }, { -2,  2 },
		{ -1, -1 }, { -2, -2 },
	},
}

--Same swipe as pulse spell
local swipe = {
	textures = { "ext/spells/pulse_attack.png" },
	width = 119,
	height = 120,
	frameCount = 6,
	duration = 400,
	offsetX = -48,
	offsetY = 13,
}

--Same as pulse spell except that it can hurt both players and enemies, and it has no flyaway "Pulse!" text
commonSpell.registerSpell("hostilePulse", {
    spellcastTargetTiles = targetTiles,
	spellcastSwipe = swipe,
	soundSpellcast = { sound = "spellPulse" },
    spellcastInflictDamage = {
        damage = 5,
        attackFlags = attack.mask(
            attack.Flag.DEFAULT,
            attack.Flag.PROVOKE,
            attack.Flag.IGNORE_TEAM
        ),
    },
	friendlyName = { name = "Pulse" },
})

--Based on firepig in Synchrony
components.register{
    pulseTrap = {
        constant.int16("litRange", 2),
		constant.int16("unlitRange", 2),
		constant.int32("collisionMask", collision.Group.WEAPON_CLEARANCE), --probably not needed anymore
		constant.int16("cooldown", 6),
		constant.int8("action", action.Special.SPELL_1),
    }
}

--based on firepig in Synchrony
local function pulseTrapComponents()
	local entity = {
		name = "PulseTrap",
		trap = {},
		trapXMLMapping = { id = 10 },
		PulseTrap_pulseTrap = {},
		collision = { mask = collision.Type.FIXTURE },
		innateSpellcasts = {
			mapping = {
				[action.Special.SPELL_1] = {
					spell = "PulseTrap_hostilePulse",
				},
			},
		},
		beatDelay = {},
		aggro = {},
		targetNearestHostileEntity = {},
		actionDelay = {},
		despawnOnWallRemoval = {},
		sprite = {
			texture = "mods/PulseTrap/gfx/PulseTrap.png",
			width = 24,
			height = 24,
		},
		normalAnimation = { frames = { 1 } },
		tellAnimation = {
			frames = { 2 },
			maxDelay = 1,
		},
		actionDelayAnimation = { frames = { 3 } },
		spriteVibrate = { active = false },
		spriteVibrateOnActionDelay = {},
		team = { id = team.Id.ENEMY },
	}
	enemy.basicComponents(entity, 0, -4, 30)
	return entity
end

customEntities.register(pulseTrapComponents())

--Checks if a target is in range of the pulse trap
--This returns true if its in the X pulse pattern
local function checkChargeTarget(entity, targetPos, maxDistance)
    local x, y = entity.position.x, entity.position.y
	local dx, dy = targetPos.x - x, targetPos.y - y
	local distance = max(abs(targetPos.x - x), abs(targetPos.y - y))
    return distance <= maxDistance and abs(dx) == abs(dy)
end

event.turn.override("processFirepigs", {sequence = 1}, function (func, ev)
    func(ev) --process actual fire pigs
    --now, process the pulse traps
    --This is mostly copied from the firepig code in Synchrony, and tries to imitate as much as possible 
    for entity in ecs.entitiesWithComponents {"PulseTrap_pulseTrap"} do 
        if entity.gameObject.tangible and entity.aggro.active and entity.beatDelay.counter == 0 then
			local lit = vision.isLit(entity.position.x, entity.position.y)
			local maxDistance = lit and entity.PulseTrap_pulseTrap.litRange or entity.PulseTrap_pulseTrap.litRange and lit or entity.PulseTrap_pulseTrap.unlitRange
			for _, target in ipairs(targeting.getHostileEntities(entity)) do
				if target and target.gameObject.active and checkChargeTarget(entity, target.position, maxDistance) then
					entity.target.entity = target.id
					entity.beatDelay.counter = entity.PulseTrap_pulseTrap.cooldown
					entity.actionDelay.currentAction = entity.PulseTrap_pulseTrap.action
				end
			end
		end

    end
end)

--Generates the pulse trap by replacing a randomly selected wall with a tier 3 wall and then placing the pulse trap on top of it if we are in zone 4
--Any wall can be replaced by a T3 wall and pulse trap except:
--Walls with dig resistance 3 or 4
--Walls in the 7 by 7 square around the spawn location (this is the starting room in zone 4)
--Cracked Walls
event.levelLoad.add("spawnPulseTrap", {order = "entities", sequence = 1}, function(ev)
	if currentLevel.getZone() ~= 4 or currentLevel.getFloor() == 4 then
		return
	end
	local xstart = ev.segments[1].bounds[1]
	local ystart = ev.segments[1].bounds[2]
	local width = ev.segments[1].bounds[3]
	local height = ev.segments[1].bounds[4]

	local x,y 
	local numAttempts = 0
	--Makes 15 attempts to place the wall
	while numAttempts < 15 do
		x = xstart + rng.int(width)
		y = ystart + rng.int(height)

		if abs(x) > 3 or abs(y) > 3 then --reject tiles in the spawn room
			if tile.getInfo(x,y).digResistance and tile.getInfo(x,y).digResistance < 3 then --Reject T3 or T4 walls, or tiles that have no walls / invincible walls
				if string.sub(tile.getInfo(x,y).friendlyName,0,7) ~= "Cracked" then	--kind of a kludge but this is the best way to check for cracked walls as far as I know			
					tile.setType(x,y,"CatacombWall")
					object.spawn("PulseTrap_PulseTrap", x,y)
					break
				end
			end
		end
		numAttempts = numAttempts + 1
	end
end)
